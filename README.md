Automation UI testing framework with Selenium
SOvTech Contact us automation test with Selenium WebDriver and TestNG.

Dependencies
Java
Maven
Selenium WebDriver
TestNG
WebDriverManager Bonigacia
Improvements
Test methods can return page intances for easy chaining
Using spreadsheets to read data for input


How to run---->
clone/download zip file of the project
Open on eclipse of IDE of choice
Click File > Import.
Type Maven in the search box under Select an import source:
Select Existing Maven Projects.
Click Next.
Click Browse and select the folder that is the root of the Maven project
Click Next.
Click Finish.

Right click on the project 
Click run as 
Click maven clean
click maven install

