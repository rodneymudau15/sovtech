package com.selenium.pages.sovTech;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.selenium.utilities.Utility;


public class ContactUs {

	/**

     * All WebElements are identified by @FindBy annotation
 
     */

    WebDriver driver;
    Utility util;
  
    @FindBy(xpath="//input[contains(@name, 'your_name')]")
    WebElement NameField;
    
    @FindBy(xpath="//label[contains(@data-reactid, '$your_name.3.$0.0')]")
    WebElement NameFieldError;
  
    @FindBy(xpath="//input[contains(@name, 'email')]")
    WebElement EmailField;    
    
    @FindBy(xpath="//label[contains(@data-reactid, '$mobilephone') and contains(@class, 'hs-error-msg')]")
    WebElement phoneFieldError;
    
    @FindBy(xpath="//label[contains(@data-reactid, 'email.3.$0.0')]")
    WebElement EmailFieldError;  

    @FindBy(xpath="//input[contains(@name, 'mobilephone')]")
    WebElement mobilephoneField;    
    
    @FindBy(xpath="//select[contains(@name, 'numemployees')]")
    WebElement companySizeNumField; 
    
    @FindBy(xpath="//label[contains(@data-reactid, '$numemployees.3.$0.0')]")
    WebElement companySizeNumFieldError;  
    
    @FindBy(xpath="//select[contains(@name, 'what_kind_of_problem_is')]")
    WebElement what_kind_of_problemField; 
    
    @FindBy(xpath="//*[starts-with(@id,'message-')]")
    WebElement messageField; 
    
    @FindBy(xpath="//input[contains(@name, 'LEGAL_CONSENT')]")
    WebElement legalConsentButton; 
    
    @FindBy(xpath="//label[contains(@data-reactid, '.hbspt-forms-0.4.0.0.0')]")
    WebElement AllFieldsError; 
    
    @FindBy(xpath="//div[contains(@class, 'section-title-wrap' )]/h2")
    WebElement successContactMessage; 
    
    @FindBy(xpath="//input[contains(@value, 'Submit' ) and contains(@type, 'submit' )]")
    WebElement submitButton; 
    public ContactUs(WebDriver driver, Utility util){

        this.driver = driver;
        this.util = util;
        //This initElements method will create all WebElements

        PageFactory.initElements(driver, this);
    }
    
	
    //Set name in Field

    public void setName(String strName) throws IOException{ 	
    	
    	Utility.sendKeys(NameField, driver, strName, this.util);
    }

    //Set email in email Field

    public void setEmail(String workEmail) throws IOException{

    	Utility.sendKeys(EmailField, driver, workEmail, this.util);
    }
    
    //Set contact number in contact Field

    public void setContact(String contactNumber) throws IOException{

    	Utility.sendKeys(mobilephoneField, driver, contactNumber, this.util);
    	
    }
    
    //Set message

    public void setMessage(String message) throws IOException{

    	Utility.sendKeys(messageField, driver, message, this.util);
    }
    
    //Set company size in company size Field

    public void setCompanySize(String companySize) throws IOException{

    	Utility.selectDropdown(companySize,companySizeNumField, driver, this.util);
    	
    }
    //Set service looking for

    public void setServiceLookingFor(String service) throws IOException{

    	Utility.selectDropdown(service,what_kind_of_problemField, driver, this.util);
    }

    //Click on Legal consent button

    public void clickLegalconsentButton(String consent) throws IOException{
    	
    	
    	if(consent.equalsIgnoreCase("yes")) {
    		Utility.clickElement(legalConsentButton, this.driver, this.util);
    	}
    	

    }  
    
    //Click on Legal consent button

    public void clicksubmitButton(){
    	
    	Utility.clickElement(submitButton, this.driver, this.util);

    } 


    /**

     * This POM method will be exposed in test case to login in the application

     * @param name

     * @param email

     * 
     * @param contact number
     * 
     
     * @param contact number
     * 
     * @param  companysize
     * 
     * @param contactservice
     * 
     * @param message
     * 
     * @param consent
     * 
     * @param expectedErrors

     */

    public void contactUs(String strName,String strEmail,String strContactNum,String strCompanySize,String strService, String strMessage, String consent, String expectedErrors) throws Exception{
    	driver.switchTo().frame(0);
    	//Fill name
        this.setName(strName);

    	
        
        //Fill Email
        this.setEmail(strEmail);

    	
        //Fill contact number
        this.setContact(strContactNum);
        
        //Fill contact number
        this.setCompanySize(strCompanySize);
        
        //Fill service
        this.setServiceLookingFor(strService); 
        
        //Fill service
        this.setMessage(strMessage);
        

        //Click Submit button  
        this.clickLegalconsentButton(consent);
         
         //Click Submit button  
         this.clicksubmitButton();
      
         
        
         //validate empty fields validation message
         if(expectedErrors.contains("name") && expectedErrors.contains("email") && expectedErrors.contains("size")) {
        	 
        	 if(companySizeNumFieldError.getText().equalsIgnoreCase("Please select an option from the dropdown menu.")
        			&& EmailFieldError.getText().equalsIgnoreCase("Please complete this required field.")
        			&& NameFieldError.getText().equalsIgnoreCase("Please complete this required field.") 
        	) {
        		 util.pass("Validation of empty fields passed",false,false);
        	 }else {
        		 util.fail("Expected Errors not displayed",false);
        	 }
         }
         //validate email validation message
         if(expectedErrors.equalsIgnoreCase("email")) {
        	 if(EmailFieldError.getText().equalsIgnoreCase("Email must be formatted correctly.")) {
        		 util.pass("Validation of email field passed",false,false); 
        	 }else {
        		 util.fail("Expected Email validation Error not displayed",false);
        	 }
        	

         }
         //validate phone validation message
         if(expectedErrors.equalsIgnoreCase("phone")) {
         	
        	 if(phoneFieldError.getText().equalsIgnoreCase("Must contain only numbers, +()-. and x.")
        			 
        	) {
        		 util.pass("Validation of phone field passed",false,false); 
        	 }else {
        		 util.fail("Expected Phone validatio Error not displayed",false);
        	 }

         }
         //switch to the main window
         driver.switchTo().defaultContent();
         //validate the successful submit message
         Thread.sleep(6000);
         if(expectedErrors.isEmpty() && successContactMessage.getText().equalsIgnoreCase("Submission Successful")) {
        	 util.pass("Valid Submition passed",false,false); 
         }else if(expectedErrors.isEmpty()&& !successContactMessage.getText().equalsIgnoreCase("Submission Successful")){
        	 util.fail("Error",false);
         }
    }
    
}