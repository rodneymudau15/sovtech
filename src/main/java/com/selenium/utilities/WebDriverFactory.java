package com.selenium.utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import io.github.bonigarcia.wdm.managers.InternetExplorerDriverManager;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
public class WebDriverFactory {
    /* Browsers constants */
    public static final String CHROME = "chrome";
    public static final String FIREFOX = "firefox";
    public static final String INTERNET_EXPLORER = "ie";
    public static final String SAFARI = "safari";

    private WebDriverFactory(){}

    
    /**
	 * 
	 * @author MD SADAB SAQIB
	 * <p>26-Jan-2021</p>
	 * @param browser : pass the desired browser and return the webdriver for the same
	 * @return WebDriver varible based on run mode provided in {@link com.testnepal.enums.ConfigProperties}
	 * @throws MalformedURLException  : Thrown to indicate that a malformed URL has occurred. Either nolegal protocol could be found in a specification string or thestring could not be parsed.
	 */
	public static WebDriver getInstance(String browser) throws MalformedURLException {
		WebDriver driver = null;

		if(browser.equalsIgnoreCase(CHROME)) {
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
			
		} 
		else if(browser.equalsIgnoreCase("FIREFOX")) {	
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			
		} 
		else if(browser.equalsIgnoreCase("edge")) {	
				WebDriverManager.edgedriver().setup();
				driver = new EdgeDriver();
			
		}
		else {
            throw new IllegalArgumentException(" Unsupported browser!");
        }

		return driver;
	}
}
