package com.selenium.mavenproject.tests.sovTech;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.selenium.utilities.Browser;
import com.selenium.utilities.Utility;
import com.selenium.utilities.WebDriverFactory;
import com.selenium.pages.sovTech.ContactUs;


public class ContactUsTest {
private WebDriver driver;
	
	ExtentTest parentTest;

	ContactUs contact;
    private Utility util = new Utility();
    private String strName = "";
    private String strEmail = "";
    private String strContact = "";
    private String strCompanySize = "";
    private String strService = "";
    private String strMessage = "";
    private String strLegalConsent = "";
    private String strRunTest = "";
    private String strTestName = ""; 
    private String expectedErrors = ""; 
   
    
    @BeforeTest
	public void setup() throws Exception{
    	
     	//obj.getCellData("BlazeDemo","Registration","TestID",0);
    	Browser.initialize(WebDriverFactory.CHROME);
        
        driver = Browser.Driver();
        
        String className = this.getClass().getName();
        util.createReport();
        parentTest = util.createTest(className);
        
	        
	        
	}
    
    @Test
    public void test_contactUS_feature() throws Exception{
    	
        //Create contact-us Page object
    	contact = new ContactUs(driver, util);
    	util.setSheet("sovTechData", "ContactUs");
    	
    	
	    for(int i = 1; i < util.rowSize(); i++) {
	    	
	    	load(i);	    	
	    	if(strRunTest.equalsIgnoreCase("true")) {
	    		Browser.goTo("https://www.sovtech.co.za/contact-us/");
	    		Thread.sleep(6000);
	    		util.createNode(parentTest,strTestName);
	    		
	    		contact.contactUs(strName, strEmail, strContact, strCompanySize, strService, strMessage, strLegalConsent, expectedErrors);
	    	}
	    }
	    
    }
    
    @AfterTest
    public void AterTest() throws IOException {
    	util.fis.close();
    	util.getInstance().flush();
    	Browser.close();
    }
    
    //Load the test data from the excel and prepare it for execution.
    public void load(int i) throws Exception {
        strName = util.getCellData("name",i);
        strEmail = util.getCellData("email",i);
        strContact = util.getCellData("contactNumber",i);
        strCompanySize = util.getCellData("companySize",i);
        strService = util.getCellData("service",i);
        strMessage = util.getCellData("message",i);
        strLegalConsent = util.getCellData("legalConsent",i);
        strRunTest = util.getCellData("RunTest",i);
        strTestName = util.getCellData("Test name",i);
        expectedErrors = util.getCellData("expectedErrors",i);
    }
}
